const {
   RtmClient,
   CLIENT_EVENTS,
   RTM_EVENTS,
   WebClient
} = require('@slack/client')

const botToken = process.env.BOT_KEY || '' // eslint-disable-line no-undef

if (!botToken) {
   console.log("Error: Couldn't load token")
   process.exit(1)
}

// Cache of data
const appData = {}

// Initialize the RTM client with the recommended settings. Using the defaults for these
const rtm = new RtmClient(botToken, {
   dataStore: false, // dataStore is deprecated, thsas
   useRtmConnect: true,
   logLevel: 'info' // debug // info // log
})

const web = new WebClient(botToken)

module.exports = { rtm, web, appData, RTM_EVENTS, CLIENT_EVENTS }
